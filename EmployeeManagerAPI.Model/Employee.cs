﻿namespace EmployeeManagerAPI.Model
{
    public class Employee
    {
        public Employee() { }

        public Employee(string name, string email, string department)
        {
            this.Name = name;
            this.Email = email;
            this.Department = department;
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
    }
}
