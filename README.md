# Employee Manager API - Luiza Labs
API for interaction with the employees database on Luizalabs. With list, create and remove options.

# Prerequisites
Clone this repository to a machine with:

- Windows OS (7+)
- .NET Framework (4.5+)
- SQL Server Express (2012+)
- Visual Studio (2012+)

# Configuration
- Adjust the connectiong strings on both web api configuration file (web.config) and unit test configuration file (app.config);
- Run the database creation script located on the solution folder to create the structure used on the API;
- Start the API from Visual Studio using any browser. You should see a default page with some examples of usage;
- The API is ready for use (the default port configured is 53238, but it can be changed if needed).

# Examples
- List employees 		- GET 		- http://localhost:53238/api/Employee/?pageSize=10&size=1
- Create new employee 	- POST 		- http://localhost:53238/api/Employee (send a json with name, email and department properties)
- Remove an employee 	- DELETE 	- http://localhost:53238/api/Employee/1 (send the ID database field)
	
# Built With
- Web API 2.0
- .NET Framework 4.5.2

# Author
Thalles Prado Ribeiro