﻿using EmployeeManagerAPI.Business;
using EmployeeManagerAPI.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace EmployeeManagerAPI.Tests
{
    [TestClass]
    public class APITests
    {
        [TestMethod]
        public void ListEmployeesReturnsListOfEmployees()
        {
            var employees = EmployeeManagerBusiness.GetEmployees(1000, 1);
            Assert.IsInstanceOfType(employees, typeof(List<Employee>));
        }

        [TestMethod]
        public void ListEmployeesReturnsAllProperties()
        {
            var employees = EmployeeManagerBusiness.GetEmployees(1000, 1);
            var employee = employees.First();
            Assert.IsNotNull(employee.Name);
            Assert.IsNotNull(employee.Email);
            Assert.IsNotNull(employee.Department);
        }

        [TestMethod]
        public void ListEmployeesLimitedByTen()
        {
            var employees = EmployeeManagerBusiness.GetEmployees(10, 1);
            Assert.IsTrue(employees.Count <= 10);
        }

        [TestMethod]
        public void ListEmployeesLimitedBy20()
        {
            var employees = EmployeeManagerBusiness.GetEmployees(20, 1);
            Assert.IsTrue(employees.Count <= 20);
        }

        [TestMethod]
        public void ListEmployeesFirstPage()
        {
            var employees = EmployeeManagerBusiness.GetEmployees(1, 1);
            Assert.IsTrue(employees.Count <= 1);
        }

        [TestMethod]
        public void ListEmployeesSecondPage()
        {
            var employees = EmployeeManagerBusiness.GetEmployees(1, 2);
            Assert.IsTrue(employees.Count <= 1);
        }

        [TestMethod]
        public void CreateEmployeeReturnsEmployeeCreated()
        {
            var newEmployee = EmployeeManagerBusiness.InsertEmployee(new Employee("API Test", "api.test@outlook.com", "Testing Department"));
            Assert.AreEqual(newEmployee.Name, "API Test");
            Assert.AreEqual(newEmployee.Email, "api.test@outlook.com");
            Assert.AreEqual(newEmployee.Department, "Testing Department");
        }

        [TestMethod]
        public void DeleteEmployeeAfterCreating()
        {
            EmployeeManagerBusiness.DeleteEmployee(2);
            Assert.IsTrue(true);
        }
    }
}
