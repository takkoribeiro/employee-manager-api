﻿using EmployeeManagerAPI.Business;
using EmployeeManagerAPI.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace EmployeeManagerAPI.Controllers
{
    public class EmployeeController : ApiController
    {
        [System.Web.Http.HttpGet]
        public System.Web.Http.Results.JsonResult<List<Employee>> Get(int pageSize, int page)
        {
            List<Employee> employees = EmployeeManagerBusiness.GetEmployees(pageSize, page);
            var result = new System.Web.Http.Results.JsonResult<List<Employee>>(employees, new JsonSerializerSettings(), Encoding.UTF8, this.ControllerContext.Request);
            return result;
        }

        [System.Web.Http.HttpPost]
        public System.Web.Http.Results.JsonResult<Employee> Post([FromBody]Employee employee)
        {
            Employee employeeInserted = EmployeeManagerBusiness.InsertEmployee(employee);
            var result = new System.Web.Http.Results.JsonResult<Employee>(employeeInserted, new JsonSerializerSettings(), Encoding.UTF8, this.ControllerContext.Request);
            return result;
        }

        [System.Web.Http.HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            bool deletion = EmployeeManagerBusiness.DeleteEmployee(id);
            
            return new HttpResponseMessage(deletion ? System.Net.HttpStatusCode.OK : System.Net.HttpStatusCode.NoContent);
        }
    }
}
