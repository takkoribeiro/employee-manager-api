﻿using EmployeeManagerAPI.Data;
using EmployeeManagerAPI.Model;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace EmployeeManagerAPI.Business
{
    public static class EmployeeManagerBusiness
    {
        private const int EMPLOYEE_NAME_MINIMUM_LENGTH = 5;
        private const int EMPLOYEE_EMAIL_MINIMUM_LENGTH = 8;
        private const int EMPLOYEE_DEPARTMENT_MINIMUM_LENGTH = 4;
        private const string NAME_REGEX = @"^[\p{L}\s'.-]+$";
        private const string EMAIL_REGEX = @"\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b";
        private const string DEPARTMENT_REGEX = @"^[\p{L}\s'.-]+$";

        public static List<Employee> GetEmployees(int pageSize, int page)
        {
            return new EmployeeManagerData().GetEmployees(pageSize, page);
        }

        public static Employee InsertEmployee(Employee employee)
        {
            if (string.IsNullOrWhiteSpace(employee.Name) || string.IsNullOrWhiteSpace(employee.Email) || string.IsNullOrWhiteSpace(employee.Department))
                throw new ArgumentException("The name, email and department fields are required.");

            if (employee.Name.Length < EMPLOYEE_NAME_MINIMUM_LENGTH || employee.Email.Length < EMPLOYEE_EMAIL_MINIMUM_LENGTH || employee.Department.Length < EMPLOYEE_DEPARTMENT_MINIMUM_LENGTH)
                throw new ArgumentException(string.Format("The name, email and department fields requires a minimum length of {0}, {1}, {2} respectively.", 
                    EMPLOYEE_NAME_MINIMUM_LENGTH, EMPLOYEE_EMAIL_MINIMUM_LENGTH, EMPLOYEE_DEPARTMENT_MINIMUM_LENGTH));

            if (!Regex.IsMatch(employee.Name, NAME_REGEX, RegexOptions.IgnoreCase))
                throw new FormatException("The name is not valid.");

            if (!Regex.IsMatch(employee.Email, EMAIL_REGEX, RegexOptions.IgnoreCase))
                throw new FormatException("The email is not valid.");

            if (!Regex.IsMatch(employee.Department, DEPARTMENT_REGEX, RegexOptions.IgnoreCase))
                throw new FormatException("The department is not valid.");

            return new EmployeeManagerData().InsertEmployee(employee);
        }

        public static bool DeleteEmployee(int id)
        {
            if (id <= 0)
                throw new ArgumentOutOfRangeException("id", "The id field is required.");

            return new EmployeeManagerData().DeleteEmployee(id);
        }
    }
}
