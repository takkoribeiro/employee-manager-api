﻿using EmployeeManagerAPI.Model;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace EmployeeManagerAPI.Data
{
    public class EmployeeManagerData
    {
        public EmployeeManagerData() {}

        private string connectionString = ConfigurationManager.ConnectionStrings["EmployeeManager"].ConnectionString; 

        public List<Employee> GetEmployees(int pageSize, int page)
        {
            List<Employee> employees = new List<Employee>();
            SqlConnection connection;
            SqlCommand command;

            //It could be a procedure in more complex applications
            string sql = string.Format("SELECT ID, NAME, EMAIL, DEPARTMENT FROM EmployeeManager.dbo.EMPLOYEE ORDER BY ID OFFSET {0} * ({1} - 1) ROWS FETCH NEXT {2} ROWS ONLY;", pageSize, page, pageSize);
            connection = new SqlConnection(connectionString);
            connection.Open();
            command = new SqlCommand(sql, connection);

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
                employees.Add(ReadEmployee(reader));

            command.Dispose();
            connection.Close();

            return employees;
        }

        public Employee InsertEmployee(Employee newEmployee)
        {
            SqlConnection connection;
            SqlCommand command;

            //It could be a procedure in more complex applications
            string sql = string.Format("INSERT INTO EmployeeManager.dbo.EMPLOYEE (NAME, EMAIL, DEPARTMENT, REGISTERDATE) VALUES ('{0}','{1}','{2}',GETDATE());", newEmployee.Name, newEmployee.Email, newEmployee.Department);
            connection = new SqlConnection(connectionString);
            connection.Open();
            command = new SqlCommand(sql, connection);

            command.ExecuteNonQuery();

            command.Dispose();
            connection.Close();

            return newEmployee;
        }

        public bool DeleteEmployee(int id)
        {
            SqlConnection connection;
            SqlCommand command;

            //It could be a procedure in more complex applications
            string sql = string.Format("DELETE FROM EmployeeManager.dbo.EMPLOYEE WHERE ID = {0};", id);
            connection = new SqlConnection(connectionString);
            connection.Open();
            command = new SqlCommand(sql, connection);

            int count = command.ExecuteNonQuery();

            command.Dispose();
            connection.Close();

            return count > 0;
        }

        private Employee ReadEmployee(SqlDataReader reader)
        {
            Employee employee = new Employee();
            employee.Name = reader["NAME"].ToString();
            employee.Email = reader["EMAIL"].ToString();
            employee.Department = reader["DEPARTMENT"].ToString();
            return employee;
        }
    }
}
